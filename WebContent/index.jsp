<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Вариант №11</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<h1>Вариант №11</h1>
	<h1>Задание</h1>
	<p style="text-align: start;">Предложить пользователю ввести строку. Конкатенировать строку с хранимой на
сервере и отобразить пользователю. Продолжать циклически пока длина строки не превысит 255
символов. После этого перестать отображать предложение для ввода и не обрабатывать запросы
от пользователя на добавление новых символов к хранимой строке.</p>

	<h1>Решение</h1>
	<p style="text-align: center">
		Хранимая строка на сервере: 
		хранимая_строка_на_сервере
	</p>
	<c:choose>
		<c:when test="${ not isStrInputted }">
			<form action="handler">
				<label for="input-str">Введите любую строку:</label>
				<input type="text" name="input-str" id="input-str">
				<input type="submit" value="Отравить">
			</form>
		</c:when>
		<c:otherwise>
			<p>
				Вы ввели строку: 
				<%=session.getAttribute("input-str") %>
			</p>
			<p>
				Строка, полученная в результате конкатенации: ${sessionScope.resStr }
			</p>
		</c:otherwise>
	</c:choose>
</body>
</html>