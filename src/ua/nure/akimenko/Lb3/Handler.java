package ua.nure.akimenko.Lb3;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/handler")
public class Handler extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static final String STORED_STR = "хранимая_строка_на_сервере";
	
    public Handler() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
								throws ServletException, IOException {
			var session = request.getSession();
			String inpStr = request.getParameter("input-str");
			session.setAttribute("input-str", inpStr);
			session.setAttribute("isStrInputted", true);
			System.out.println("Session id = " + session.getId());
			var sb = new StringBuilder();
			sb.append(STORED_STR);
			while (sb.length() < 255) {
				sb.append(inpStr);
			}
			session.setAttribute("resStr", sb.toString());
			response.sendRedirect(
					request.getContextPath()
			);
		}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
